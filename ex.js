console.log("Hello World!")
let a;
a="a string";
console.log(a);
a=12;
console.log(typeof(a));

for(var i=0;i<=5;i++)
console.log(i);

for (let j=0;j<=3;j++)
console.log(j);


let c=[1,2,3,4,5];
console.log(c);

c[3]=10;
console.log(c);

c.push(13);
console.log(c);

c.pop();
console.log(c);

c.shift();
console.log(c);

console.log(c.indexOf(2));
console.log(c.indexOf(3));

let s="i'm a little string";
console.log(s.split(' '));
console.log(s.split(' ').join('-'));

let o={
	name: "a name",
	age: 35,
//console.log(o);
printme: function()
{
	console.log(`${this.name} is ${this.age} old`);
}
}
o.printme();

// definire functii

function f(x)
{
	return x+2;
}
console.log(f(4));

let student={
	varsta: 20,
	nume: "Popescu",
	grupa: 1081,
	media: 8,
	nota1: 8,
	nota2: 7,
	nota3: 8,

	printeaza: function()
	{
		console.log(`Studentul cu numele ${this.nume} are varsta ${this.varsta} se afla in grupa ${this.grupa} si are media ${this.media}`);
	},
	media: function()
	{
		console.log((this.nota2+this.nota3+this.nota1)/3);
	}


}
student.printeaza();
student.media();

function media(x,y)
{
	return (x+y)/2;
}
console.log(media(8,9));

let g=function(x)
{
	return x+6;
}
console.log(g(5));

let g1=(x)=>{
	return x+4;
}
console.log(g1(4));

let g2=(x,y,z)=>
{
	return x+y+z +2;
}
console.log(g2(1,2,3));
let g3=(y)=>{
	return y+8;
}
console.log(g3(2));

let g4=(x,y)=>x/y;
console.log(g4(4,2));

